import React from 'react';
import image1 from '../images/image6.jpeg';
import image2 from '../images/image7.jpeg';

const Services = () => (
  <>
  <section id='content'>
  <h1>Services & Costs</h1>
  Your dog shall receive the full-length time it deserves. Check for our local branch and pop in if you’re unsure which service is right for you. Every do is unique, meaning that every dog deserves our full attention 7 days a week.
  <section id="services">
    <h3>  Dog Walking </h3>
    <h4>  £12/h </h4>
    <h4>  Duration: 1 Hour (enquire for more)</h4>
    <h5>  Not all dogs enjoy group walks, therefore we provide two different services. Solo or 	group, discover which option is best for your dog.</h5>
  </section>
  <section id="services">
    <h3>House calls</h3>
    <h4>£15/h</h4>
    <h4>Duration: 1 Hour (enquire for more)</h4>
    <h5>Need your dog looking after while you’re not home? This service allows you to have peace of mind while we ensure that your dog has the attention and care it deserves.</h5>
  </section>
  <section id="services">
    <h3>Day Care</h3>
    <h4>£60/day</h4>
    <h4>Duration: 8 hours per day</h4>
    <h5>Our professional day care centers provice the most up to date and clean facilities which ensures that your dog is safe, protected and healthy. This will cover services from feeding, bathing walking and sleeping.</h5>
</section>
</section>
<img src={image1} id="image6"/>
<img src={image2} id="image7"/>
  </>
);

export default Services;
