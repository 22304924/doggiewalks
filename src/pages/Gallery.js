import React from 'react';
import image1 from '../images/image1.jpg';
import image2 from '../images/image2.jpeg';
import image3 from '../images/image3.jpeg';
import image4 from '../images/image4.jpeg';
import image10 from '../images/image10.jpeg';
import image11 from '../images/image11.jpeg';
import image12 from '../images/image12.jpeg';
import image13 from '../images/image13.jpeg';

const Gallery = () => (

  <>
  <section id="content">
  <h1>Gallery</h1>
  <h3>
    Every week is a busy week! Checkout how much fun our guests have within our day care centers. All photos have been taken with the consent of every owner.
  </h3>
  </section>
  <section id="gallery">
  </section>
    <img src={image1} id="image1"/>
    <img src={image2} id="image2"/>
    <img src={image3} id="image3"/>
    <img src={image4} id="image4"/>
    <img src={image10} id="image10"/>
    <img src={image11} id="image11"/>
    <img src={image12} id="image12"/>
    <img src={image13} id="image13"/>

  </>
);

export default Gallery;
