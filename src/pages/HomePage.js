

import React from 'react';
import image from '../images/image9.jpeg';
import image2 from '../images/image5.jpeg'
import image3 from '../images/image14.jpg';
import image4 from '../images/paw.jpg';
import image5 from '../images/paws.jpg';

const HomePage = () => (
  <>
  <header>
    <div id="offer">
      <h2> 50% OFF HOME VISITS TO NEW CUSTOMERS </h2>
      <img src={image4} id="paw" />
    </div>

    <div id="cf">
      <img src={image} id="image9" class="bottom"/>
      <img src={image3} id="image9" class="top"/>
      </div>
  </header>

  <section id="home-content">
    <h1>  Welcome to Doggie Walks! </h1>
    <h3>We are committed to providing professional and personal care to your beloved four-legged friends.</h3>
    <p>
      We are based in Ormskirk we offer exciting and stimulating walks all over the beautiful Lancashire and Merseyside countryside. We guarantee your dog will come home tired and content.
    </p>
    <p>
      We recognise every dog’s needs are different and how busy daily life can be. With services starting from only £10 we ensure you the best possible service to suit the needs of both you and your pooch. We are also fully insured for everybody’s peace of mind, for more information on this please <b> contact us</b> or email at info@doggiewalks.co.uk
    </p>
    <img src={image2} id="image5"/>
    <p>
      There’s something to suit everyone but if you can’t find what you’re looking for don’t hesitate to contact us and we’ll find the perfect solution for you and your companion!
    </p>
  </section>

  <div id="float1">
    <aside>
     <h3> Locations we operate in: </h3>
          <h4> &#128062;  Ormskirk </h4>
          <h4> &#128062;    Merseyside </h4>
          <h4> &#128062;  Lancashire </h4>
    </aside>
  </div>

  <div id="float2">
    <aside>
      <h4>
      We also offer different services such as:
      </h4>
      <p> &#128062; Pet sitting (Puppy and Elderly)</p>
      <p> &#128062; Small pet visits</p>
      <p> &#128062; Overnight Pet Sitting</p>
    </aside>
  </div>

  </>
);

export default HomePage;
