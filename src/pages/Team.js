import React from 'react';
import image1 from '../images/team/lori-hanson.jpg';
import image2 from '../images/team/rebecca-harrison.jpg';
import image3 from '../images/team/gary-simpson.jpg';
import image4 from '../images/team/joanna-davies.jpg';
import image5 from '../images/paws.jpg'

const Team = () => (
  <>
  <section id="content">
  <h1>Meet The Team!</h1>
  <p>
  Meet the doggie walks team! We’re fully trained and insured to work with animals and are commited to the health and benefits of your pet. We aim to keep providing the best knowledge regarding dogs as we can, and to do this we believe you cannot sit still in this occupation as we learn more about dogs daily. We ensure that our knowledge is kept up-to-date by attending regular seminars and courses presented by well-respected canine trainers, behaviourists and practitioners.
  </p>
  </section>
  <section>
  <aside>
    <img src={image1} id="team1"/>
    <p>
      <h3>Lori Hanson</h3>
      <h4>Founder of Doggie Walks</h4>
      </p>
    <img src={image3} id="team1"/>

      <h3>Gary Simpson</h3>
      <h4>Team Manager</h4>

    <img src={image2} id="team1"/>

      <h3>Rebecca Harrison</h3>
      <h4>Professional Dog Walker</h4>

    <img src={image4} id="team1"/>

      <h3>Joanna Davies</h3>
      <h4>Professional Dog Walker</h4>
      </aside>
      </section>
      <div>
        <img src={image5} id="paws" />
      </div>
  </>
);

export default Team;
