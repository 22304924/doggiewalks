import React from 'react';
import { Link } from 'react-router-dom';
import { ReactComponent as Logo } from './images/logo.svg';

const NavBar = () => (
  <>

  <nav id="nav">
      <Logo  id="logo"/>
      <p id="slogan"> Helping our animal friends one paw at a time.</p>
      <ul>
        <li>
          <Link to='/'>Home &#128062;</Link>
        </li>
        <li>
          <Link to="/about">About Us &#128062;</Link>
        </li>
        <li>
          <Link to="/team">Meet The Team &#128062;</Link>
        </li>
        <li>
          <Link to="/services">Services & Costs &#128062;</Link>
        </li>
        <li>
          <Link to="/gallery">Gallery &#128062;</Link>
        </li>
        <li>
          <Link to="/contact">Contact Us &#128062;</Link>
        </li>
      </ul>

    </nav>
    </>
);

export default NavBar;
