import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => (
  <>
  <footer id="footer">
    <div id="times">
    <p> Opening Times:</p>
    <p>
    Monday - Friday: 8am till 7pm
    </p><p>
    Saturday: 8am till 5pm
    </p><p>
    Sunday: 12noon till 5pm
    </p>
    </div>
    <div id="email">
    <p>Contact information: <a href="mailto:someone@example.com">info@doggiewalks.co.uk</a></p><p> Phone: 01695 883 112</p>
    </div>
  </footer>
    </>
);

export default Footer;
